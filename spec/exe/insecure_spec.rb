# frozen_string_literal: true

RSpec.describe "insecure", :aggregate_failures do
  it "greets" do
    expect do
      expect(
        system("./exe/insecure", exception: true)
      ).to eq true
    end.to output("Hello World!\n").to_stdout_from_any_process
  end
end
