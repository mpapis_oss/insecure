# frozen_string_literal: true

RSpec.describe Insecure do
  it "greets" do
    expect { described_class.greet }.to output("Hello World!\n").to_stdout
  end
end
