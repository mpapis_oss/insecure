# frozen_string_literal: true

RSpec.describe Insecure::VERSION do
  it "is a version number" do
    expect(Insecure::VERSION).not_to be nil
  end
end
