# frozen_string_literal: true

require_relative "insecure/version"

# InSecure AS within security.
# Generate authentication and authorization application code for your company.
module Insecure
  class Error < StandardError; end

  def self.greet
    puts "Hello World!"
  end
end
